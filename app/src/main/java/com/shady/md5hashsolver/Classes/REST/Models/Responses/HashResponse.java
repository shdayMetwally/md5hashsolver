package com.shady.md5hashsolver.Classes.REST.Models.Responses;


import lombok.Getter;
import lombok.Setter;

@Setter
@Getter
public class HashResponse {
    private String name;
    private String email;
    private String hash;

}
