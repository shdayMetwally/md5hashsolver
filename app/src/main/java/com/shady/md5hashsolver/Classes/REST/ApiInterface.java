package com.shady.md5hashsolver.Classes.REST;


import com.shady.md5hashsolver.Classes.REST.Models.Requests.HashRequest;
import com.shady.md5hashsolver.Classes.REST.Models.Responses.HashResponse;

import io.reactivex.Observable;
import retrofit2.http.Body;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.GET;
import retrofit2.http.POST;
import retrofit2.http.PUT;
import retrofit2.http.Path;


public interface ApiInterface {

    @PUT("hash/{slug}")
    Observable<HashResponse> requestHash(@Path("slug") String slug,
                                         @Body HashRequest request);


}
