package com.shady.md5hashsolver.Classes.Utils;

import android.os.Bundle;
import android.util.Log;

import androidx.appcompat.app.AppCompatActivity;

import com.shady.md5hashsolver.R;

import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.ArrayList;
import java.util.List;

import javax.inject.Inject;

import timber.log.Timber;

public class HashSolver {
    private String myEmailHash;

    String[] symbols = {"a", "b", "c", "d", "e", "f", "g", "h", "i", "j", "k", "l", "m", "n", "o", "p", "q", "r", "s", "t", "u", "v", "w", "x", "y", "z",
            "0", "1", "2", "3", "4", "5", "6", "7", "8", "9",
            "+", "-", ".", "_", "@"
    };
    String email = "";

    @Inject
    public HashSolver() {

    }

    public String solve(String myEmail,String hash) {
        myEmailHash = md5(myEmail);

        List<String> hashes = new ArrayList<>();
        for (int start = 0; start < hash.length(); start += 32) {
            hashes.add(hash.substring(start, Math.min(hash.length(), start + 32)));
        }

        for (String hashPart : hashes) {
            email = findHashLetters(hashPart);
        }
        Timber.d("Shady solve: %s", email);
        return email;
    }

    private String findHashLetters(String hash) {
        for (String firstSymbol : symbols) {
            for (String secondSymbol : symbols) {
                String letters = email + firstSymbol + secondSymbol;
                if (md5(myEmailHash + letters + md5(letters)).equals(hash)) {
                    return letters;
                }
            }
        }
        return "";
    }

    public static String md5(final String s) {
        final String MD5 = "MD5";
        try {
            // Create MD5 Hash
            MessageDigest digest = MessageDigest
                    .getInstance(MD5);
            digest.update(s.getBytes());
            byte messageDigest[] = digest.digest();

            // Create Hex String
            StringBuilder hexString = new StringBuilder();
            for (byte aMessageDigest : messageDigest) {
                String h = Integer.toHexString(0xFF & aMessageDigest);
                while (h.length() < 2)
                    h = "0" + h;
                hexString.append(h);
            }
            return hexString.toString();

        } catch (NoSuchAlgorithmException e) {
            e.printStackTrace();
        }
        return "";
    }
}