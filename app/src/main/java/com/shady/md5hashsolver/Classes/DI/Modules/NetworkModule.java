package com.shady.md5hashsolver.Classes.DI.Modules;


import android.location.Location;

import androidx.lifecycle.MutableLiveData;


import com.shady.md5hashsolver.Classes.Others.CONSTANTS;
import com.shady.md5hashsolver.Classes.REST.ApiInterface;
import com.shady.md5hashsolver.Classes.REST.Models.Responses.HashResponse;

import java.util.List;

import dagger.Module;
import dagger.Provides;
import dagger.hilt.InstallIn;
import dagger.hilt.android.components.ViewModelComponent;
import dagger.hilt.android.scopes.ViewModelScoped;
import io.reactivex.disposables.CompositeDisposable;
import okhttp3.OkHttpClient;
import okhttp3.logging.HttpLoggingInterceptor;
import retrofit2.Retrofit;
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory;
import retrofit2.converter.gson.GsonConverterFactory;
import timber.log.Timber;


@Module
@InstallIn(ViewModelComponent.class)
public class NetworkModule {

    @Provides
    @ViewModelScoped
    public ApiInterface provideApiInterface(OkHttpClient okHttpClient) {
        return new Retrofit.Builder()
                .baseUrl(CONSTANTS.BACKEND.BASE_URL)
                .addConverterFactory(GsonConverterFactory.create())
                .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
                .client(okHttpClient)
                .build()
                .create(ApiInterface.class);

    }


    @Provides
    @ViewModelScoped
    public OkHttpClient okHttpClient(HttpLoggingInterceptor httpLoggingInterceptor) {
        return new OkHttpClient.Builder()
                .addInterceptor(httpLoggingInterceptor)
                .build();

    }
    @Provides
    @ViewModelScoped
    public HttpLoggingInterceptor provideLoggingInterceptor() {
        HttpLoggingInterceptor httpLoggingInterceptor = new HttpLoggingInterceptor(Timber::i);
        httpLoggingInterceptor.setLevel(HttpLoggingInterceptor.Level.BODY);
        return httpLoggingInterceptor;
    }


    @Provides
    public CompositeDisposable provideCompositeDisposable() {
        return new CompositeDisposable();
    }


    @Provides
    public MutableLiveData<HashResponse> provideHashResponseMutableLiveData() {
        return new MutableLiveData<>();
    }


}
