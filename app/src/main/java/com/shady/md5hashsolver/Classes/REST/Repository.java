package com.shady.md5hashsolver.Classes.REST;


import com.shady.md5hashsolver.Classes.REST.Models.Requests.HashRequest;
import com.shady.md5hashsolver.Classes.REST.Models.Responses.HashResponse;

import javax.inject.Inject;

import dagger.hilt.android.scopes.ViewModelScoped;
import io.reactivex.Observable;

@ViewModelScoped
public class Repository {
    private static String fireBaseToken;
    private final ApiInterface apiInterface;

    @Inject
    public Repository(ApiInterface apiInterface) {
        this.apiInterface = apiInterface;
    }


    public Observable<HashResponse> requestHash(HashRequest request) {
        return apiInterface.requestHash(request.getSlug(), request);
    }


}

