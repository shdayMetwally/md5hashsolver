package com.shady.md5hashsolver.Classes.REST.Models.Requests;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.Setter;

@Setter
@Getter
@Builder
@AllArgsConstructor
public class HashRequest {
    private transient String slug;
    private String GUID;

}
