package com.shady.md5hashsolver.MVVM.Main;

import androidx.lifecycle.MutableLiveData;
import androidx.lifecycle.ViewModel;

import com.shady.md5hashsolver.Classes.REST.Models.Requests.HashRequest;
import com.shady.md5hashsolver.Classes.REST.Models.Responses.HashResponse;
import com.shady.md5hashsolver.Classes.REST.Repository;

import java.io.IOException;
import java.net.InetSocketAddress;
import java.net.Socket;
import java.net.SocketAddress;

import javax.inject.Inject;

import dagger.hilt.android.lifecycle.HiltViewModel;
import io.reactivex.Observable;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.schedulers.Schedulers;
import lombok.Getter;
import lombok.Setter;
import timber.log.Timber;


@Setter
@Getter
@HiltViewModel
public class MainViewModel extends ViewModel {
    @Inject
    public MainViewModel() {
    }

    @Inject
    Repository repository;
    @Inject
    CompositeDisposable compositeDisposable;

    @Inject
    MutableLiveData<HashResponse> hashResponseMutableLiveData;


    public void requestHash(HashRequest request) {
        getCompositeDisposable().add(Observable.just(request)
                .observeOn(Schedulers.io())
                .takeWhile(this::isInternetAvailable)
                .flatMap(data -> getRepository().requestHash(data))
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(response -> hashResponseMutableLiveData.setValue(response), Timber::d));
    }


    public boolean isInternetAvailable(Object object) {
        try {
            int timeoutMs = 5000;
            Socket sock = new Socket();
            SocketAddress sockaddr = new InetSocketAddress("8.8.8.8", 53);

            sock.connect(sockaddr, timeoutMs);
            sock.close();

            return true;
        } catch (IOException e) {
            return false;
        }
    }

    @Override
    protected void onCleared() {
        super.onCleared();
        compositeDisposable.clear();
    }

}
