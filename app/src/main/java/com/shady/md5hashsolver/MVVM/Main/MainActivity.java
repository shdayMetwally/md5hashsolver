package com.shady.md5hashsolver.MVVM.Main;

import static com.shady.md5hashsolver.Classes.Others.CONSTANTS.BACKEND.GUID;
import static com.shady.md5hashsolver.Classes.Others.CONSTANTS.BACKEND.SLUG;

import android.os.Bundle;

import androidx.appcompat.app.AppCompatActivity;
import androidx.lifecycle.ViewModelProvider;

import com.shady.md5hashsolver.Classes.REST.Models.Requests.HashRequest;
import com.shady.md5hashsolver.Classes.REST.Models.Responses.HashResponse;
import com.shady.md5hashsolver.Classes.Utils.HashSolver;
import com.shady.md5hashsolver.R;
import com.shady.md5hashsolver.databinding.ActivityMainBinding;

import javax.inject.Inject;

import dagger.hilt.android.AndroidEntryPoint;

@AndroidEntryPoint
public class MainActivity extends AppCompatActivity {
    private ActivityMainBinding mBinding;
    private MainViewModel mViewModel;

    @Inject
    HashSolver hashSolver;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        mBinding = ActivityMainBinding.inflate(getLayoutInflater());
        setContentView(mBinding.getRoot());

        mViewModel = new ViewModelProvider(this).get(MainViewModel.class);
        mViewModel.getHashResponseMutableLiveData().observe(this, this::onHashResponse);

        mViewModel.requestHash(HashRequest.builder()
                .slug(SLUG)
                .GUID(GUID)
                .build());


    }

    private void onHashResponse(HashResponse response) {
        String email = hashSolver.solve(response.getEmail(), response.getHash());
        mBinding.tvEmail.setText(email);
    }

}